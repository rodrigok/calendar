//
//  CLDDayViewController.h
//  calendar
//
//  Created by Rodrigo Nascimento on 09/01/14.
//  Copyright (c) 2014 Rodrigo Nascimento. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLDDayViewController : UIViewController

@property NSInteger day;
@property NSInteger month;

@property (strong, nonatomic) IBOutlet UILabel *holidayLabel;

@end
