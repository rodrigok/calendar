//
//  CLDDayViewController.m
//  calendar
//
//  Created by Rodrigo Nascimento on 09/01/14.
//  Copyright (c) 2014 Rodrigo Nascimento. All rights reserved.
//

#import "CLDDayViewController.h"
#import "CLDUtils.h"

@interface CLDDayViewController ()

@end

@implementation CLDDayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CLDUtils *cldUtils = [[CLDUtils alloc] init];
    
    NSString *holiday = [cldUtils getHolidayDescriptionOf:self.month andOf:self.day];
    
    if (holiday != nil) {
        self.holidayLabel.text = holiday;
    } else {
        self.holidayLabel.text = @"Dia sem feriado";
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
