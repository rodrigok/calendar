//
//  CLDDayDetailViewController.h
//  calendar
//
//  Created by Rodrigo Nascimento on 08/01/14.
//  Copyright (c) 2014 Rodrigo Nascimento. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLDMonthViewController : UIViewController;

@property NSInteger month;

@end
