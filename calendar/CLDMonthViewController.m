//
//  CLDDayDetailViewController.m
//  calendar
//
//  Created by Rodrigo Nascimento on 08/01/14.
//  Copyright (c) 2014 Rodrigo Nascimento. All rights reserved.
//

#import "CLDMonthViewController.h"
#import "CLDDayViewController.h"
#import "CLDUtils.h"

@interface CLDMonthViewController ()

@end

@implementation CLDMonthViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CLDUtils *cldUtils = [[CLDUtils alloc] init];
    

    NSNumber *days = [NSNumber numberWithInteger:[cldUtils getDaysOf:self.month]];
    NSInteger rows = ceil(days.floatValue / 7);

    NSInteger tabBarHeight = self.tabBarController.tabBar.bounds.size.height;
    NSInteger navBarHeight = self.navigationController.navigationBar.bounds.size.height;
    NSInteger statusBarHeight = [UIApplication sharedApplication].statusBarFrame.size.height;

    NSNumber *buttonWidth = [NSNumber numberWithFloat:(self.view.frame.size.width - 2) / 7];
    NSNumber *buttonHeight = [NSNumber numberWithFloat:(self.view.frame.size.height - tabBarHeight - navBarHeight - statusBarHeight - 2) / rows];

    for (int i = 0; i < [days integerValue]; i++) {
        NSNumber *col = [NSNumber numberWithFloat:fmod(i, 7)];
        NSInteger line = floor(i/7);
        
        CGRect buttonFrame = CGRectMake(col.floatValue * buttonWidth.floatValue + 1, line * buttonHeight.floatValue + 1 + navBarHeight + statusBarHeight, buttonWidth.floatValue - 1, buttonHeight.floatValue - 1);
        UIButton *button = [[UIButton alloc] initWithFrame:buttonFrame];
        
        [button setTitle:[NSString stringWithFormat:@"%d", i + 1] forState:UIControlStateNormal];
        NSString *holiday = [cldUtils getHolidayDescriptionOf:self.month andOf:i + 1];
        
        [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        
        [button setTag:i + 1];
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];

        [self.view addSubview:button];
        
        if (holiday) {
            UILabel *point = [[UILabel alloc] initWithFrame:CGRectMake(buttonFrame.origin.x, buttonFrame.origin.y + 10 + buttonFrame.size.height / 2, buttonFrame.size.width, 20)];
            [point setText:@"●"];
            [point setTextAlignment:NSTextAlignmentCenter];
            [point setTextColor:[UIColor lightGrayColor]];
            [point setFont:[UIFont systemFontOfSize:30]];
            [self.view addSubview:point];
        }

    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    CLDDayViewController *view = (CLDDayViewController *) segue.destinationViewController;
    
    UIButton *button = sender;
    
    view.navigationItem.title = button.titleLabel.text;
    
    view.day = button.tag;
    view.month = self.month;
}

- (void)buttonClicked: (id)sender {
    [self performSegueWithIdentifier:@"dayDetail" sender:sender];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
