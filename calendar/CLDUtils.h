//
//  CLDUtils.h
//  calendar
//
//  Created by Rodrigo Nascimento on 08/01/14.
//  Copyright (c) 2014 Rodrigo Nascimento. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLDUtils : NSObject

- (NSString *) getMonthNameFrom: (int)monthNumber;
- (NSInteger) getDaysOf: (int)monthNumber;
- (NSString *) getHolidayDescriptionOf: (int)monthNumber andOf: (int)dayNumber;

@end
