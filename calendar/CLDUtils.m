//
//  CLDUtils.m
//  calendar
//
//  Created by Rodrigo Nascimento on 08/01/14.
//  Copyright (c) 2014 Rodrigo Nascimento. All rights reserved.
//

#import "CLDUtils.h"

@implementation CLDUtils {
}

- (NSDate *) getDateFrom: (int)monthNumber {
    NSDate *date = [[NSDate alloc] init];
    NSDateComponents *components = [[NSCalendar currentCalendar] components: NSYearCalendarUnit fromDate:date];

    NSString *dateString = [NSString stringWithFormat: @"%d-%d", [components year], monthNumber];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM"];

    return [dateFormatter dateFromString:dateString];
}

- (NSString *) getMonthNameFrom: (int)monthNumber {
    NSDate* myDate = [self getDateFrom:monthNumber];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM"];
    
    return [formatter stringFromDate:myDate];
}

- (NSInteger) getDaysOf: (int)monthNumber {
    NSDate *myDate = [self getDateFrom:monthNumber];
    
    NSCalendar *c = [NSCalendar currentCalendar];
    NSRange days = [c rangeOfUnit:NSDayCalendarUnit
                           inUnit:NSMonthCalendarUnit
                          forDate:myDate];
    
    return days.length;
}

- (NSString *) getHolidayDescriptionOf: (int)monthNumber andOf: (int)dayNumber {
    NSDictionary *holidays = @{
                               @"1": @{
                                       @"1": @"Réveillon"
                                       },
                               @"2": @{
                                       @"2": @"Nossa Senhora dos Navegantes"
                                       },
                               @"3": @{
                                       @"4": @"Carnaval"
                                       },
                               @"4": @{
                                       @"17": @"Quinta-Feira Santa",
                                       @"18": @"Sexta-Feira Santa",
                                       @"19": @"Sábado de Aleluia",
                                       @"20": @"Páscoa",
                                       @"21": @"Tiradentes"
                                       },
                               @"5": @{
                                       @"1": @"Dia do Trabalho"
                                       },
                               @"6": @{
                                       @"12": @"Copa do Mundo",
                                       @"17": @"Copa do Mundo",
                                       @"18": @"Copa do Mundo",
                                       @"19": @"Corpus Christi",
                                       @"23": @"Copa do Mundo",
                                       @"25": @"Copa do Mundo",
                                       @"30": @"Copa do Mundo"
                                       },
                               @"9": @{
                                       @"7": @"Independência do Brasil",
                                       @"20": @"Revolução Farroupilha"
                                       },
                               @"10": @{
                                       @"12": @"Nossa Senhora Aparecida",
                                       @"15": @"Dia do Professor"
                                       },
                               @"11": @{
                                       @"2": @"Finados",
                                       @"15": @"Proclamação da República"
                                       },
                               @"12": @{
                                       @"25": @"Natal"
                                       }
                               };
    
  
    
    NSString *monthString = [NSString stringWithFormat:@"%d", monthNumber];
    NSString *dayString = [NSString stringWithFormat:@"%d", dayNumber];
    
    if ([holidays objectForKey:monthString] == nil) {
        return nil;
    }
    
    return [[holidays objectForKey:monthString] objectForKey:dayString];
}

@end
