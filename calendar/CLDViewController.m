//
//  CLDViewController.m
//  calendar
//
//  Created by Rodrigo Nascimento on 08/01/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "CLDViewController.h"
#import "CLDMonthViewController.h"
#import "CLDUtils.h"

@interface CLDViewController ()

@end

@implementation CLDViewController {
    CLDUtils *cldUtils;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    cldUtils = [[CLDUtils alloc] init];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 12;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Month";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    [[cell textLabel] setText:[cldUtils getMonthNameFrom:indexPath.row + 1]];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    CLDMonthViewController *view = (CLDMonthViewController *) segue.destinationViewController;
    
    view.navigationItem.title = [cldUtils getMonthNameFrom:[[self.tableView indexPathForSelectedRow] row] + 1];
    
    view.month = [[self.tableView indexPathForSelectedRow] row] + 1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
