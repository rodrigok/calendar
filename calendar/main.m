//
//  main.m
//  calendar
//
//  Created by Rodrigo Nascimento on 08/01/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CLDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CLDAppDelegate class]));
    }
}
